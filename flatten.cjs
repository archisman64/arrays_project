function flatten(elements, depth) {
    if(!Array.isArray(elements)) {
        return [];
    }
    let flat = [];
    if(depth === undefined) {
        depth = 1;
    }
    function helper(elements, flat, start, depth) {
        console.log('helper called with depth', depth);
        for(let index = start; index < elements.length; index ++) {
            // console.log('current element:', elements[index]);
            if(index in elements) {
                if(Array.isArray(elements[index]) && depth){
                    // console.log('found nesting');
                    // found a nested element; start iterating inside it from it's first index
                    helper(elements[index], flat, 0, depth-1);
                } else {
                    // console.log('found flat');
                    // element is not nested so push it into resultant flat array
                    flat.push(elements[index]);
                    // console.log('flat object:', flat);
                }
            }
        }
    }
    helper(elements, flat, 0, depth);
    return flat;
}

module.exports = flatten;