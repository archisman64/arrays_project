function filter(elements, cb) {
    if(!elements) {
        return [];
    }
    // flag will be either true or false depending on the callback's satifying condition
    let flag;
    // the array will contain either the satisfying elements or none of them
    let result = [];
    for(let index = 0; index < elements.length; index ++) {
        flag = cb(elements[index], index, elements);
        // if flag is true that means out element meets the stisfying condition
        // and we'll push it to the result array
        if(flag === true){
            result.push(elements[index]);
        }
    }
    // result array will be empty if no element matches the callback's condition
    return result;
}

module.exports = filter;