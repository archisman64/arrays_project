function find(elements, cb) {
    let flag;
    for(let index = 0; index < elements.length; index ++) {
        flag = cb(elements[index]);
        if(flag) {
            return elements[index];
        }
    }
    return undefined;
}

module.exports = find;