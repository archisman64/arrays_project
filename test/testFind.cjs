const find = require('../find.cjs');

const testArray = [10,2,3,4, 20];

function callback(element) {
    return element > 10;
}

console.log(find(testArray, callback));