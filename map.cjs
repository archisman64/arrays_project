function map(elements, cb) {
    let mapped = [];
    if(!Array.isArray(elements) || elements.length === 0) {
        return [];
    }
    for(let index = 0; index < elements.length; index ++) {
        mapped[index] = cb(elements[index], index, elements);
    }
    return mapped;
}

module.exports = map;