function reduce(elements, cb, startingValue) {
    let indexStart;
    if(!startingValue && startingValue !== 0) {
        startingValue = elements[0];
        indexStart = 1;
    } else {
        startingValue = startingValue;
        indexStart = 0;
    }
    if(elements.length === 0) {
        return startingValue;
    }
    for(let index = indexStart; index < elements.length; index ++) { 
        startingValue = cb(startingValue, elements[index], index, elements);
        // console.log('accumulator value after', index + 1, 'run', startingValue);
    }
    return startingValue;
}

module.exports = reduce;