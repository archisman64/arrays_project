function each(elements, cb) {
    if(!Array.isArray(elements) || elements.length === 0) {
        return [];
    }
    for(let index = 0; index < elements.length; index ++) {
        cb(elements[index], index);
    }
    return elements;
}

module.exports = each;